from cars import get_country_of_a_car_brand, get_gas_usage_for_distance


def test_get_country_for_a_japanese_car():
    assert get_country_of_a_car_brand("Suzuki") == "Japan"


def test_get_country_for_japanese_cars_lowercase():
    assert get_country_of_a_car_brand("toyota") == "Japan"


def test_get_country_for_a_germany_car():
    assert get_country_of_a_car_brand("Audi") == "Germany"


def test_get_country_for_germany_cars_lowercase():
    assert get_country_of_a_car_brand("audi") == "Germany"


def test_get_country_for_a_french_car():
    assert get_country_of_a_car_brand("Citroen") == "France"


def test_get_country_for_french_cars_lowercase():
    assert get_country_of_a_car_brand("citroen") == "France"


def test_get_country_for_a_italian_car():
    assert get_country_of_a_car_brand("fiat") == "Unknown"


# Tests for a counting calculating gas usage
def test_gas_usage_for_100_kilometers_distance_driven():
    assert get_gas_usage_for_distance(100, 8.5) == 8.5


def test_gas_usage_for_zero_kilometers_distance_driven():
    assert get_gas_usage_for_distance(0, 8.5) == 0


def test_gas_usage_for_negative_distance_driven():
    assert get_gas_usage_for_distance(-50, 8.5) == 0


def test_gas_usage_for_zero_gas_usage_driven():
    assert get_gas_usage_for_distance(50, 0) == 0


def test_gas_usage_for_negative_gas_usage_driven():
    assert get_gas_usage_for_distance(50, -10) == 0
