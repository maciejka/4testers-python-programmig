import random
import string
import uuid


# Exercise 2
def create_random_password_uuid(email):
    user_dict = {'email': email, 'password': str(uuid.uuid4())}
    return user_dict


def create_a_random_password(user_email, password_length):
    characters = string.ascii_lowercase + string.digits + string.punctuation
    password = ''.join(random.choice(characters) for i in range(password_length))
    user_data = {
        "email": user_email,
        "password": password
    }
    if '@' in user_email:
        print(user_data)
    else:
        print("This is not an email address!")


def player_description(player_dictionary):
    print(
        f"The player {player_dictionary['nick']} is of type {player_dictionary['type']}"
        f" and has {player_dictionary['exp_points']} EXP")


player_1 = {
    "nick": "maestro_54",
    "type": "warrior",
    "exp_points": 3000
}


def run_exercise_2():
    create_a_random_password("anka@lo.pl", 25)
    print(f"User password:{create_random_password_uuid('fjak@kfd.pl')}")


def run_exercise_3():
    player_description(player_1)


if __name__ == '__main__':
    run_exercise_2()
    run_exercise_3()
