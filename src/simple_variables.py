my_name = "Bernadeta"
my_age = 34
my_email = "bmaciejuk@gmail.com"

print(my_name)
print(my_age)
print(my_email)

# Below si the description of my friend

my_friend_name = "Kamil"
my_friends_age = 35
my_friends_number_of_pets = 0
if_he_has_driving_license = True
age_of_friendship = 10

print(my_friend_name, my_friends_age, my_friends_number_of_pets, if_he_has_driving_license, age_of_friendship, sep="\t")


