import datetime
import random

# Listy zawierające dane do losowania

female_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
male_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']


def generate_personal_dictionary(is_male):
    if is_male:
        first_name = male_fnames
        first_name = random.choice(male_fnames)
    else:
        first_name = female_fnames
        first_name = random.choice(female_fnames)

    age = random.randint(5, 45)
    last_name = random.choice(surnames)
    country = random.choice(countries)
    current_year = int(datetime.datetime.now().strftime("%G"))
    year_of_birth = current_year - age
    is_adult = True if age >= 18 else False
    return {
        'first_name': first_name,
        'last_name': last_name,
        'country': country,
        'email': (first_name + '.' + last_name + '@example.com').casefold(),
        'age': age,
        'is_an_adult': is_adult,
        'year_of_birth': year_of_birth
    }


if __name__ == '__main__':
    list_of_dictionaries = []
    for i in range(10):
        if i % 2 == 0:
            list_of_dictionaries.append(generate_personal_dictionary(True))
        else:
            list_of_dictionaries.append(generate_personal_dictionary(False))
        print(
            f'Hi! I\'m {list_of_dictionaries[i]["first_name"]} {list_of_dictionaries[i]["last_name"]}. I come from {list_of_dictionaries[i]["country"]} and I was born in {list_of_dictionaries[i]["year_of_birth"]}, \n')
