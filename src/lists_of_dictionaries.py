if __name__ == '__main__':
    addresses = [
        {
            "city": "Warsaw",
            "street": "Maliali",
            "house_number": "2",
            "post_code": "15-061"
        },
        {
            "city": "Cracow",
            "street": "Liczna",
            "house_number": "4",
            "post_code": "16-4144"
        },
        {
            "city": "Poznan",
            "street": "Wesola",
            "house_number": "4",
            "post_code": "15-065"
        },
        {
            "city": "Opole",
            "street": "Mila",
            "house_number": "56",
            "post_code": "18-25"
        },
    ]


print(f"Post code from the last addres: {addresses[-1]['post_code']}")
print(f"City from the last addres: {addresses[1]['city']}")

addresses[0]['street'] = "Złota"

print(addresses)
