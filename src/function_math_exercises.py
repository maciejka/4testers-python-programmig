# Exercise 1
def calculate_the_square_of_the_number(number):
    return number * number


print(calculate_the_square_of_the_number(0))
print(calculate_the_square_of_the_number(16))
print(calculate_the_square_of_the_number(2.55))


# Exercise 2
def volume_of_the_cuboid(a, b, c):
    return a * b * c


print(volume_of_the_cuboid(3, 5, 7))


# Exercise 3
def convert_temperature_in_celsius_to_fahrenheit(celsius):
    return celsius * 9/5 + 32


print(convert_temperature_in_celsius_to_fahrenheit(20))


def convert_temperature_in_fahrenheit_to_celsius(fahrenheit):
    return (fahrenheit - 32) * 5/9


print(convert_temperature_in_fahrenheit_to_celsius(250))
