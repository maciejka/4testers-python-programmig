# Exercise 1

friend = {
    'name': 'Kamil',
    'age': 35,
    'hobbies': ['football', 'music']
}
print(friend['age'])
friend['city'] = 'Wrocław'
print(friend)
del(friend['age'])
print(friend['hobbies'][-1])

