animal = {
    "kind": "dog",
    "age": 2,
    "male": True
}
animal2 = {
    "kind": "cat",
    "age": 5,
    "male": False
}
animal3 = {
    "kind": "fish",
    "age": 1,
    "male": True
}
animal_kind = ["dog", "cat", "fish"]

animals = [
    {
        "kind": "dog",
        "age": 2,
        "male": True
    },
    {
        "kind": "dog",
        "age": 2,
        "male": False
    }
]
print(animals[0])
print(animals[-1])
last_animal = animals[-1]
last_animal_age = last_animal["age"]
print(last_animal_age)
print(animals[-1]["age"])
print(animals[0]["kind"])


animals.append(
    {
        "kind": "dolphin",
        "age": 6,
        "male": False
    }
)
print(animals)