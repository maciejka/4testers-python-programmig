import uuid


def print_temperature_description(celsius):
    print(f"Temperature right now is {celsius} Celsius degrees")
    if celsius > 25:
        print("It's getting hot!")
    elif celsius > 0:
        print("It's quite Ok")
    else:
        print("It's getting cold")


def is_person_an_adult(age):
    # if age >= 18:
    #     return True
    # else:
    #     return False
    #
    # Ternary operator
    return True if age >= 18 else False


def weather_conditions(temperature, air_pressure):
    if temperature == 0 and air_pressure == 1013:
        return True
    else:
        return False


def print_random_uuids(number_of_strings):
    for i in range(number_of_strings):
        print(str(uuid.uuid4()))


def print_numbers_multiplied_by_4(from_number, to_number, step):
    for number in range(from_number, to_number + 1, step):
        print(number * 4)


def print_numbers_divisible_by_7(from_number, to_number):
    for number in range(from_number, to_number + 1):
        if number % 7 == 0:
            print(number)


if __name__ == "__main__":
    temperature_in_celsius = 28
    print_temperature_description(temperature_in_celsius)

    print(weather_conditions(0, 1013))
    print(weather_conditions(0, 1014))
    print(weather_conditions(1, 1014))

    print_random_uuids(10)
    print_numbers_multiplied_by_4(4, 100, 2)

    print_numbers_divisible_by_7(4, 98)
