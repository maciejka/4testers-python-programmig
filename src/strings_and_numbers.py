first_name = "Bernadeta"
last_name = "Makowska"
email = "bmaciejuk@gmail.com"

my_bio = "Mam na imie " + first_name + ". Moje nazwisko to " + last_name + ". Mój email to " + email + "."

print(my_bio)

# F-string
my_bio_using_f_string = f"Mam na imie {first_name}.\nMoje imie to {last_name}.\nMój email to {email}."
print(my_bio_using_f_string)

print(f"Wynik operacji mnożenia 4 przez 5 to {4*5}")

### Algebra ###
circle_radius = 4
area_of_a_circle = 3.1415 * circle_radius ** 2
circumference_of_a_circle = 2 * 3.14 * circle_radius

print(f"Area of a circle with radius {circle_radius}:", area_of_a_circle)
print(f"Circumference of a circle with radius {circle_radius}:", circumference_of_a_circle)

long_mathenatical_expression = 2 + 3 + 5 * 8 + (5 * 9) + 7 ** 3
print(long_mathenatical_expression)

