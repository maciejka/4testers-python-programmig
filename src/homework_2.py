from functools import reduce


def get_average_of_list(list_with_numbers):
    return sum(list_with_numbers) / len(list_with_numbers)


def get_average_of_two_lists(avg_temperatures1, avg_temperatures2):
    return (avg_temperatures1 + avg_temperatures2) / 2


def run_exercise_1():
    temperatures_in_january = [-4, 1.0, -7, 2]
    temperatures_in_february = [-13, -7, -2, 0]

    average_temperature = get_average_of_two_lists(get_average_of_list(temperatures_in_january),
                                                   get_average_of_list(temperatures_in_february))
    print(f"The average tamperature of two months is", average_temperature)


if __name__ == '__main__':
    run_exercise_1()

# temperatures_in_february_len = len(temperatures_in_february)
# temperatures_in_january_len = len(temperatures_in_january)
# temperatures_in_february_avg = reduce(lambda x, y: x + y, temperatures_in_february) / temperatures_in_february_len
# temperatures_in_january_avg = reduce(lambda x, y: x + y, temperatures_in_january) / temperatures_in_january_len
# print(f'The average temperature in february is: {temperatures_in_february_avg}')
# print(f'The average temperature in january is: {temperatures_in_january_avg}')
