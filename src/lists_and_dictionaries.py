shopping_list = ['oranges', 'water', 'chicken', 'tomatoes', 'potatoes']
print(shopping_list[0])
print(shopping_list[1])
print(shopping_list[2])
print(shopping_list[-1])
print(shopping_list[-2])
shopping_list.append('lemons')
print(shopping_list)

number_of_items_to_by = len(shopping_list)
print(number_of_items_to_by)

first_three_shopping_items = shopping_list[0:3]
print(first_three_shopping_items)


animal = {
    "name": "Burek",
    "kind": "dog",
    "age": 5,
    "male": True
}

dog_age = animal["age"]
print("Dog age: ", dog_age)
dog_name = animal["name"]
print("Dog name is", dog_name)

animal["age"] = 10
print(animal)
animal["owner"] = "Julek"
print(animal)




