def convert_celsius_to_fahrenheit(temperatures_in_celsius):
    temperatures_in_fahrenheit = []
    for temperature in temperatures_in_celsius:
        fahrenheit = round(temperature * 9 / 5 + 32, 2)
        temperatures_in_fahrenheit.append(fahrenheit)
    print(f"List of temperatures in Celsius degree:", temperatures_in_celsius)
    print(f"List of temperatures in Fahrenheit degree:", temperatures_in_fahrenheit)


def conversion_from_celsius_to_fahrenheit(temperatures_in_fahrenheit):
    temperatures_in_fahrenheit = []
    for temperature in temperatures_in_fahrenheit:
        fahrenheit = round(temperature * 9 / 5 + 32, 2)
        temperatures_in_fahrenheit.append(fahrenheit)
    return temperatures_in_fahrenheit


if __name__ == '__main__':

    temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    convert_celsius_to_fahrenheit(temps_celsius)
    print(conversion_from_celsius_to_fahrenheit(temps_celsius))
