# Exercise 1 - Print welcome message to a person in a city
def print_hello_message(name, city):
    print(f"Witaj {name.capitalize()}! Milo Cię widziec w naszym miescie: {city.capitalize()}!")


print_hello_message("Michal", "torun")
print_hello_message("Beata", "Gdynia")


# Exercise 2 - Generating email address

def generate_address_email(name, last_name):
    return f"{name.lower()}.{last_name.lower()}@4testers.pl"


print(generate_address_email("Janusz", "Nowak"))
print(generate_address_email("Barbara", "Kowalska"))





